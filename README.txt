User role mapping for Ezmlm
==================================

Ezmlm is a mailing list manager for Qmail which can be downloaded at
http://untroubled.org/ezmlm/. Ezmlm uses either PostgreSQL, MySQL or
local files to store its user lists. This module enables Drupal to
add and remove users to database user lists based upon their roles
within Drupal.

Setup
==================================
1. Enable the module under modules
2. Create the database configuration under Configuration -> Ezmlm ->
   Ezmlm database.
3. Create the mappings for your roles under Configuration -> Ezmlm ->
   Ezmlm mappings. If you do not want to map a role to a list, leave
   the default value of "none".

Under the roles you will see many tables listed. Ezmlm has some tables
for specific functions like moderator-managed lists and digest lists.
For most situations, you will want to use the root table. For example,
if your list is called "project@domain.com", you will see tables for:
  project - the main list subscribers (you probably want this one)
  project_allow - only used for lists with sender restrictions
  project_deny - only used for lists with sender restrictions
  project_digest - used for digest list subscribers
  project_mod - list moderators

Minutiae
==================================

This module requires you to use a database storage method for your user 
lists. You can mix local files and database for diferent lists but this 
module will only maintain a database table.

This module will only add and remove users when they are modified in
Drupal. It will not populate a mailing list with existing users or
remove users from the mailing list who were pre-existing and not
registered in Drupal. The only time this module will add or remove an
email from the mailing list is when the user is modified in Drupal.

If a list administrator users manual methods to add a user to the list
this module will not add that user to the role in Drupal. If the user is
in Drupal but not in the role, they will be removed from the list the next
time their account is modified. Always make sure your list users and 
managers know the appropriate way to add and remove users from your lists.

Ezmlm has the ability to remove users when probes to their email address
are failing. This function is managed by Ezmlm. If a user is removed due
to a failed probe email, this module will not readd that user until their
account is changed in Drupal.

